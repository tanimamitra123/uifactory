import React from 'react';
import PropTypes from 'prop-types';
import { DropdownButton, MenuItem, SplitButton, } from "react-bootstrap";

const AdvanceCombobox = props => {
    return (
      
        <div>
            <SplitButton bsStyle={'default'} title={'Advanced ComboBox'} id={`split-button-basic`}>
                <MenuItem eventKey="1">Action</MenuItem>
                <MenuItem divider />
                <MenuItem eventKey="2">Another action</MenuItem>
                <MenuItem divider />
                <MenuItem eventKey="3">Something else here</MenuItem>
                <MenuItem divider />
                <MenuItem eventKey="4">Separated link</MenuItem>
            </SplitButton>
        </div>
    );
  
};

AdvanceCombobox.propTypes = {

};

export default AdvanceCombobox;