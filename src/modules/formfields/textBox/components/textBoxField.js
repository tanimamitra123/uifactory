import React from 'react';
import PropTypes from 'prop-types';
import {Table} from "react-bootstrap";

const textBoxField = props => {
    return (
        <div>
                <Table striped bordered condensed hover>
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>City</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Akshay</td>
                            <td>Kharche</td>
                            <td>Pune</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Tanima</td>
                            <td>Mitra</td>
                            <td>Pune</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Prasad</td>
                            <td>Kalamkar</td>
                            <td>Pune</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
    );

};


textBoxField.propTypes = {

};
export default textBoxField;




