import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router";
import { Grid, Row, Col, Button, ButtonGroup, bsS } from 'react-bootstrap';

class RightSideBar extends Component {
    constructor(props) {
        super();
    }
    render() {
        return (
            <div>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <ul className="nav nav-tabs" role="tablist">
                            <li role="presentation" className="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">View</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Store</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Model</a></li>
                        </ul>
                    </div>
                    <div className="panel-body">
                        <div className="tab-content">
                            <div role="tabpanel" className="tab-pane active" id="home">
                                View Code here
                            </div>
                            <div role="tabpanel" className="tab-pane" id="profile">
                                Store Code here
                            </div>
                            <div role="tabpanel" className="tab-pane" id="messages">
                                Model Code here
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

RightSideBar.propTypes = {

};

export default RightSideBar;

