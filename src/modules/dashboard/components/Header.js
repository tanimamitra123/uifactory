import React  from 'react';
import {Navbar} from 'react-bootstrap';

const Header = props => {
    return <Navbar fixedTop fluid inverse>
        <Navbar.Header>
            <Navbar.Brand>
                <a href="#">UI Factory</a>
            </Navbar.Brand>
        </Navbar.Header>
    </Navbar>
};

export default Header;
