import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SimpleCombobox from '../../formfields/comboBox/components/simpleCombobox';
import TextBoxField from '../../formfields/textBox/components/textBoxField';
import Header from 'modules/dashboard/components/Header';
import LeftSideBar from 'modules/dashboard/components/LeftSideBar';
import RightSideBar from 'modules/dashboard/components/RightSideBar';
import {Link} from "react-router";
import { Grid, Row, Col, Button, ButtonGroup, bsS } from 'react-bootstrap';

const Dashboard = props => {
    return <div>
                <Grid fluid className="main-container">
                    <Row>
                        <Col md={3}>
                           <LeftSideBar/>
                        </Col>
                        <Col md={6} className="main-content">
                         <div>                               
                              {props.children} 
                         </div>
                        </Col>
                        <Col md={3}>
                          <RightSideBar/>
                          
                        </Col>
                    </Row>
                </Grid>
            </div>
};

Dashboard.propTypes = {};

export default Dashboard;

