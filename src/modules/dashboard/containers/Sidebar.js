'use strict';

import React, {Component, View} from 'react';
import {connect} from 'react-redux';
import SidebarComponent from 'modules/dashboard/components/Sidebar';
import {push} from 'react-router-redux';

class Sidebar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.gotoModule = this.gotoModule.bind(this);
    }
    componentDidMount() {
    }

    componentWillReceiveProps(props) {
    }

    gotoModule(path) {
        this.props.routeDispatch(push(path));
    }

    render() {
        return <SidebarComponent
            gotoModule={this.gotoModule}
        />
    }

}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
    routeDispatch: dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
