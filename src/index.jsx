import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import routes from 'app/routes';
import store from 'app/store';
import 'bootstrap/dist/css/bootstrap.css';
import 'resources/css/styles.css';

render(
    <Provider store={store}>
        {routes}
    </Provider>,
    document.getElementById('root')
);
