import { useRouterHistory } from 'react-router';
import { createHashHistory } from 'history';
import {createStore, applyMiddleware, compose} from 'redux';
import createLogger from 'redux-logger';
import {identity} from 'lodash';
import reducers from 'app/reducers';
import {routerMiddleware} from 'react-router-redux';
import thunk from 'redux-thunk';

const appHistory = useRouterHistory(createHashHistory)({ queryKey: false });
const devTools = window.devToolsExtension || (() => noop => noop);

const Store = createStore(
        reducers,
        compose(
            applyMiddleware(
                thunk,
                createLogger(),
                routerMiddleware(appHistory)
            ),
            devTools()
        )
    );

export default Store;

